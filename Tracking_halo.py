#!/usr/bin/env python
# coding: utf-8

import pickle
from pylab import *
import caesar
import h5py
import yt, os
from scipy.optimize import curve_fit
from scipy.stats import binned_statistic as bst
from scipy.spatial import cKDTree

sim='m100n1024'
wind='s50'

# # track progenitors
class tracking_pg(object):
    def __init__(self, ids,startsn=151,endsn=14):
        path='../'+sim+'/'+wind+'/' 
        nsnap=startsn-endsn+1
        self.snapnumber=np.arange(startsn,endsn-1,-1)
        self.reds=np.zeros(nsnap,dtype=float)

        
        # fill_progentor_data()
        for i,sn in enumerate(self.snapnumber):
            exts='000'+str(sn)
            if wind == 's50nojet' and sn in [94, 95]:
                continue
            tmpg = caesar.load(path+'Groups/'+sim+'_'+exts[-3:]+'.hdf5')
            
            if i == 0:
                if isinstance(ids, type('a')):
                    if ids == 'center':
                        cidf=np.array([l.central for l in tmpg.galaxies])
                        ids = np.where(cidf==True)[0]  # note that obj.galaxies[1000].GroupID == 1000
                self.gids = ids
                self.bhmasses=np.zeros((nsnap, ids.size),dtype=float)-1
                self.bhmdot = np.zeros((nsnap, ids.size),dtype=float)-1
                self.bh_fedd= np.zeros((nsnap, ids.size),dtype=float)-1
                self.stellarmasses=np.zeros((nsnap, ids.size),dtype=float)-1
                self.stellarmasses_2HMR=np.zeros((nsnap, ids.size),dtype=float)-1
                self.central=np.zeros((nsnap, ids.size,),dtype=np.bool_)
                self.HImasses=np.zeros((nsnap, ids.size),dtype=float)-1
                self.H2masses=np.zeros((nsnap, ids.size),dtype=float)-1
                self.gasmasses=np.zeros((nsnap, ids.size),dtype=float)-1
                self.sfr=np.zeros((nsnap, ids.size),dtype=float)-1

                self.stellar_metallicity=np.zeros((nsnap, ids.size),dtype=float)-1
                self.stellar_half_mass_radii=np.zeros((nsnap, ids.size),dtype=float)-1

                self.progen_index=np.zeros((nsnap, ids.size),dtype=np.int64)-1
                self.haloIDs=np.zeros((nsnap, ids.size),dtype=np.int64)-1
                self.halomasses_HI=np.zeros((nsnap, ids.size),dtype=float)-1
                self.halomasses_H2=np.zeros((nsnap, ids.size),dtype=float)-1
                self.halomasses_stellar=np.zeros((nsnap, ids.size),dtype=float)-1
                self.halomasses_fof=np.zeros((nsnap, ids.size),dtype=float)-1
                self.halomasses_gas=np.zeros((nsnap, ids.size),dtype=float)-1
            
            self.reds[i]=tmpg.simulation.redshift
            if sn == startsn:
                for j, jid in enumerate(ids):
                    self.bhmasses[i,j] = tmpg.galaxies[jid].masses['bh']
                    self.bhmdot[i,j] = tmpg.galaxies[jid].bhmdot
                    self.bh_fedd[i,j] = tmpg.galaxies[jid].bh_fedd
                    self.stellarmasses[i,j] = tmpg.galaxies[jid].masses['stellar']
                    self.central[i,j] = tmpg.galaxies[jid].central
                    self.HImasses[i,j] = tmpg.galaxies[jid].masses['HI']
                    self.H2masses[i,j] = tmpg.galaxies[jid].masses['H2']
                    self.gasmasses[i,j] = tmpg.galaxies[jid].masses['gas']
                    self.sfr[i,j] = tmpg.galaxies[jid].sfr

                    self.stellar_metallicity[i,j] = tmpg.galaxies[jid].metallicities['stellar']
                    self.stellar_half_mass_radii[i,j] = tmpg.galaxies[jid].radii["stellar_half_mass"]
                    
                    self.haloIDs[i,j] = tmpg.galaxies[jid].parent_halo_index
                    if len(tmpg.halos[self.haloIDs[i,j]].progen_halo_dm)>0:
                        self.progen_index[i,j] = tmpg.halos[self.haloIDs[i,j]].progen_halo_dm[0]
                    else:
                        self.progen_index[i,j] = -1
                    if self.haloIDs[i,j]>-0.5:
                        self.halomasses_HI[i,j] = tmpg.halos[self.haloIDs[i,j]].masses['HI']
                        self.halomasses_H2[i,j] = tmpg.halos[self.haloIDs[i,j]].masses['H2']
                        self.halomasses_stellar[i,j] = tmpg.halos[self.haloIDs[i,j]].masses['stellar']
                        self.halomasses_fof[i,j] = tmpg.halos[self.haloIDs[i,j]].masses['total']
                        self.halomasses_gas[i,j] = tmpg.halos[self.haloIDs[i,j]].masses['gas']
                    
            else:
                for j, jid in enumerate(self.progen_index[i-1,:]):
                    if jid != -1:
                        self.halomasses_HI[i,j] = tmpg.halos[jid].masses['HI']
                        self.halomasses_H2[i,j] = tmpg.halos[jid].masses['H2']
                        self.halomasses_stellar[i,j] = tmpg.halos[jid].masses['stellar']
                        self.halomasses_fof[i,j] = tmpg.halos[jid].masses['total']
                        self.halomasses_gas[i,j] = tmpg.halos[jid].masses['gas']
                        if len(tmpg.halos[jid].progen_halo_dm)>0:
                            self.progen_index[i,j] = tmpg.halos[jid].progen_halo_dm[0]
                        else:
                            self.progen_index[i,j] = -1
                        self.haloIDs[i,j] = jid
                        
                        if tmpg.halos[jid].central_galaxy is not None: 
                            cgalaxies=tmpg.halos[jid].central_galaxy
                            if 'bh' in  cgalaxies.masses.keys():
                                self.bhmasses[i,j] = cgalaxies.masses['bh']
                                self.bhmdot[i,j] = cgalaxies.bhmdot
                                self.bh_fedd[i,j] = cgalaxies.bh_fedd
                                
                            self.stellarmasses[i,j] = cgalaxies.masses['stellar']
                            self.central[i,j] = cgalaxies.central
                            self.HImasses[i,j] = cgalaxies.masses['HI']
                            self.H2masses[i,j] = cgalaxies.masses['H2']
                            self.gasmasses[i,j] = cgalaxies.masses['gas']
                            self.sfr[i,j] = cgalaxies.sfr
                            self.stellar_half_mass_radii[i,j] = cgalaxies.radii["stellar_half_mass"]
                            
                            if hasattr(cgalaxies, 'metallicities'):
                                if 'stellar' in cgalaxies.metallicities.keys():
                                    self.stellar_metallicity[i,j] = cgalaxies.metallicities['stellar']
                 
            del(tmpg)


gp_all = tracking_pg('center')
with open('data/tracking_center_halo_'+sim+'_'+wind+'.class', 'wb') as f:
    pickle.dump(gp_all, f)

# gp_sample.reds

