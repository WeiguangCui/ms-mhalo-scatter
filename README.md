## The data is produced from the SIMBA simulation with its Caesar catalogue, which is publicly available at http://simba.roe.ac.uk/

## The `Plots_Ms-Mh-study.ipynb` is the file to produce all the figures in the paper (can also be found in plots folder):

**DECLARATION:**

	*The data will be only used to reproduce the plots in the paper. For other usages, questions and other issues, please contact the owner (cuiweiguang@gmail.com) for permission and/or details.*

Thank you for your interests.


Weiguang Cui
